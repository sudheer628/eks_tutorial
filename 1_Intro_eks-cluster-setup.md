AWS EKS Kubernetes - Self learning - Part1
==========================================

Kubernetes is a portal, extensible, open-source platform for managing containerized workloads.

EKS core objects
----------------

EKS Cluster has below core objects.

1. EKS Control plane

    :   1.  Contains kubernetes master components like etcd, apiserver,
            kube controller.
        2.  It is managed service by AWS. EKS Runs a single tenant
            Kubernetes control plane for each cluster, and control plane
            infrastructure is not shared accross clusters or AWS
            accounts.
        3.  This control plane consist of atleast 2 API server nodes,
            and 3 etcd nodes that run accross 3 AZ within a region.
        4.  EKS automatically detects and replaces unhealthy control
            plane instances, restarting them across the AZ within a
            region as needed.

2. Worker Nodes & Node Groups

    :   1.  Group of EC2 instances where we run our application work
            loads. These are called nodes.
        2.  EKS Worker nodes run in our AWS account and connect to our
            cluster\'s control plane via the cluster API server end
            point.
        3.  A node group is one or more EC2 instances that are deployed
            in an EC2 Autoscaling Group.
        4.  All nodes in a node group must be the same instance type,
            running same AMI, and using the same EKS worker node IAM
            role.

3. Fargate Profiles

    :   1.  Instead of EC2 instances, we run our application work loads
            on serverless fargate profiles. AWS Fargate is a technology
            that provides on-demand, right sized compute capacity for
            containers.
        2.  With Fargate, we no longer have to provision, configure, or
            scale groups of virtual machines to run containers.
        3.  Each pod running on Fargate has its own isolation boundary,
            and does not share the underlying kernel, CPU, memory, or
            elastic n/w interface with other pod.
        4.  AWS specially built Fargate Controllers that recognizez the
            pods belonging to fargate and schedules them on Fargate
            profiles.
        5.  Fargate profiles run on private subnet of VPC.

4. VPC

    :   1.  With AWS VPC, we follow secure networking standards which
            will allow us to run production workloads on EKS.
        2.  EKS Uses AWS VPC n/w to restrict traffic between control
            plane components to within a single cluster.
        3.  Control plane components for EKS cluster can not view or
            receive communication from other clusters or other AWS
            accounts except as authorized with kubernetes RBAC policies.
        4.  This secure and highly available configuration makes EKS
            reliable and recommended for production workloads.

## Architecture

<p align="center">
<img src="/images/eks-arch-2.png" width="1500" height="700" >
</p>

### Master Node

#### kube-apiserver
It acts as front end for the Kubernetes control plane. It exposes the Kubernetes API
Command line tools (like kubectl), Users and even Master components (scheduler, controller manager, etcd) and Worker node components like (Kubelet) everything talk with API Server. 

#### etcd
Consistent and highly-available key value store used as Kubernetes’ backing store for all cluster data.
It stores all the masters and worker node information. 

#### kube-scheduler
Scheduler is responsible for distributing containers across multiple nodes.  
It watches for newly created Pods with no assigned node, and selects a node for them to run on.

#### kube-controller-manager

1.  Controllers are responsible for noticing and responding when nodes, containers or endpoints go down. They make decisions to bring up new containers in such cases. 
2.  Node Controller: Responsible for noticing and responding when nodes go down. 
3.  Replication Controller: Responsible for maintaining the correct number of pods for every replication controller object in the system.
4.  Endpoints Controller:  Populates the Endpoints object (that is, joins Services & Pods)
5.  Service Account & Token Controller: Creates default accounts and API Access for new namespaces.

### Worker Nodes

#### kubelet
Kubelet is the agent that runs on every node in the cluster. This agent is responsible for making sure that containers are running in a Pod on a node. 

#### Kube-Proxy
It is a network proxy that runs on each node in your cluster.
It maintains network rules on nodes
In short, these network rules allow network communication to your Pods from network sessions inside or outside of your cluster.

#### Container Runtime
Container Runtime is the underlying software where we run all these Kubernetes components. 
We are using Docker, but we have other runtime options like rkt, container-d etc.

#### kubectl 
we can control kubernetes cluster and objects using kubectl.

#### eksctl

It is very very powerful tool for managing EKS clusters on AWS. it is
used for

1.  creating & deleting clusters on AWS EKS only.
2.  we can even create, autoscale and delete node groups.
3.  we can even create fargate profiles using eksctl.

[installation documentation](https://github.com/stacksimplify/aws-eks-kubernetes-masterclass/tree/master/01-EKS-Create-Cluster-using-eksctl/01-01-Install-CLIs)


Kubernetes Fundamentals
-----------------------

<p align="center">
<img src="/images/eks-funda-1.png" width="1500" height="600" >
</p>

### Kubernetes Pod

A Pod is a single instance of an application. With kubernetes our core goal will be to deploy our applications in the form of containers on worker nodes in a k8s cluster. Kubernetes does not deploy containers on worker nodes. Container is encapsulated into a kubernetes object name POD. A POD contain multiple containers but not of same kind.

Pods generally have one to one relationship with the containers. To scale UP we create new POD and to scale down we DELETE the POD.

One worker node can contain multiple PODs. We can not have multiple containers of same kind in a single POD. example: Two NGINX containers in single POD serving same purpose is not recommended.


How does EKS Works
------------------

STEP1 - Provision a EKS Cluster (EKS Automatically deploys kubernetes
master)

STEP2 - Deploy worker nodes (Add worker nodes to your EKS cluster)

STEP3 - Connect to EKS (Point your favourite kubernetes tooling at your
EKS cluster)

STEP4 - Run kubernetes apps (Deploy applications to your EKS cluster)

### Step1 - Cluster creation

Create EKS Cluster using eksctl:

    eksctl create cluster --name=eksdemo1 \
                --region=us-east-1 \
                --zones=us-east-1a,us-east-1b \
                --without-nodegroup 


    eksctl get cluster

Note - .kube/config file is automatically created with above step.
incase of existing EKS cluster integration with local eksctl tool, use
below options.

Whatelse to create from eksctl

    eksctl create addon                           Create an Addon
    eksctl create cluster                         Create a cluster
    eksctl create fargateprofile                  Create a Fargate profile
    eksctl create iamidentitymapping              Create an IAM identity mapping
    eksctl create iamserviceaccount               Create an iamserviceaccount - AWS IAM role bound to a Kubernetes service account
    eksctl create nodegroup                       Create a nodegroup


### Step-02 - Create & Associate IAM OIDC Provider for our EKS Cluster

1.  To enable and use AWS IAM roles for Kubernetes service accounts on
    our EKS cluster, we must create & associate OIDC identity provider.
2.  To do so using [eksctl]{.title-ref} we can use the below command.
3.  Use latest eksctl version (as on today the latest version is
    [0.21.0]{.title-ref})

Template:

    eksctl utils associate-iam-oidc-provider \
        --region region-code \
        --cluster <cluter-name> \
        --approve
    # Replace with region & cluster name
    eksctl utils associate-iam-oidc-provider \
        --region us-east-1 \
        --cluster eksdemo1 \
        --approve

### Step-03: Create EC2 Keypair

1.  Create a new EC2 Keypair with name as [kube-demo]{.title-ref}
2.  This keypair we will use it when creating the EKS NodeGroup.
3.  This will help us to login to the EKS Worker Nodes using Terminal.

### Step-04: Create Node Group with additional Add-Ons in Public Subnets

These add-ons will create the respective IAM policies for us
automatically within our Node Group role.

Create Public Node Group:

    eksctl create nodegroup --cluster=eksdemo1 \
                --region=us-east-1 \
                --name=eksdemo1-ng-public1 \
                --node-type=t3.medium \
                --nodes=2 \
                --nodes-min=2 \
                --nodes-max=4 \
                --node-volume-size=20 \
                --ssh-access \
                --ssh-public-key=kube-demo \
                --managed \
                --asg-access \
                --external-dns-access \
                --full-ecr-access \
                --appmesh-access \
                --alb-ingress-access 

### Step-05: Verify Cluster & Nodes

Verify NodeGroup subnets to confirm EC2 Instances are in Public Subnet

1.  Verify the node group subnet to ensure it created in public subnets
2.  Go to Services -\> EKS -\> eksdemo -\> eksdemo1-ng1-public
3.  Click on Associated subnet in **Details** tab
4.  Click on **Route Table** Tab.
5.  We should see that internet route via Internet Gateway (0.0.0.0/0
    -\> igw-xxxxxxxx)

Verify Cluster, NodeGroup in EKS Management Console

1.  Go to Services -\> Elastic Kubernetes Service -\> eksdemo1

List Worker Nodes:

    eksctl get cluster
    # List NodeGroups in a cluster
    eksctl get nodegroup --cluster=<clusterName>
    # List Nodes in current kubernetes cluster
    kubectl get nodes -o wide
    # Our kubectl context should be automatically changed to new cluster
    kubectl config view --minify

Verify Worker Node IAM Role and list of Policies

1.  Go to Services -\> EC2 -\> Worker Nodes
2.  Click on **IAM Role associated to EC2 Worker Nodes**

Verify Security Group Associated to Worker Nodes

1.  Go to Services -\> EC2 -\> Worker Nodes
2.  Click on **Security Group** associated to EC2 Instance which
    contains [remote]{.title-ref} in the name.

Verify CloudFormation Stacks

1.  Verify Control Plane Stack & Events
2.  Verify NodeGroup Stack & Events

Login to Worker Node using Keypair kube-demo

Login to worker node:

    #For MAC or Linux or Windows10
    ssh -i kube-demo.pem ec2-user@<Public-IP-of-Worker-Node>
    # For Windows
    Use putty