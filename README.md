## Amazon Elastic Kubernetes Service (Amazon EKS) Tutorial

self learning tutorial

[Part-0](https://gitlab.com/sudheer628/eks_tutorial/-/blob/main/images/4855175-docker-cheatsheet-r4v2.pdf) docker essential commands

[Part-1](https://gitlab.com/sudheer628/eks_tutorial/-/blob/main/1_Intro_eks-cluster-setup.md) AWS EKA introduction and Setup 

[Part-2](https://github.com/) next

[Part-3](https://github.com/) next 

[Part-4](https://github.com/) next 


## Courtesy links

stacksimplify tutorial [link](https://github.com/stacksimplify/aws-eks-kubernetes-masterclass)

EKS best practices [link](https://github.com/sudheer628/aws-eks-best-practices)
